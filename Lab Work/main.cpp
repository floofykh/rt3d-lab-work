// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Cube.h"
#include <GLM\gtc\matrix_transform.hpp>
#include <GLM\gtc\type_ptr.hpp>
#include <stack>

using namespace std;

GLuint textures[2];

rt3d::materialStruct material0 = {
{0.4f, 0.2f, 0.2f, 1.0f}, // ambient
{0.8f, 0.5f, 0.5f, 1.0f}, // diffuse
{1.0f, 0.8f, 0.8f, 1.0f}, // specular
2.0f // shininess
};
rt3d::materialStruct material1 = { // material with transparency
{0.4f, 0.2f, 0.2f, 0.3f}, // ambient
{0.8f, 0.5f, 0.5f, 0.3f}, // diffuse
{1.0f, 0.8f, 0.8f, 0.3f}, // specular
2.0f  // shininess
};

GLfloat lightR, lightG, lightB;
rt3d::lightStruct light0 = {
{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
{0.8f, 0.8f, 0.8f, 1.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 1.0f}, // specular
{0.0f, 10.0f, 0.0f, 1.0f} // position
};

glm::vec4 lightPos;
GLfloat rotation;
glm::vec3 eyeTarget, eyePos, upDir;

bool using1stShader;

GLuint shaderProgram;

Cube **cubes, *base;

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	// For this simple example we'll be using the most basic of shader programs
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	using1stShader = false;
	textures[0] = rt3d::loadBitmap("fabric.bmp");
	textures[1] = rt3d::loadBitmap("studdedmetal.bmp");
	// Going to create our mesh objects here
	cubes = new Cube* [6];
	for(int i=0; i<6; i++)
	{
		cubes[i] = new Cube(glm::vec3(i*2.0f, 0.0f, -4.0f), glm::vec3(1.0f), glm::vec3(1.0f));
		cubes[i]->init();
		cubes[i]->setMaterial(material0);
		cubes[i]->setTexture(textures[0]);
	}

	base = new Cube(glm::vec3(0.0f, -0.5f, -4.0f), glm::vec3(20.0f, 0.1f, 20.0f), glm::vec3(1.0f));
	base->init();
	base->setMaterial(material0);
	base->setTexture(textures[1]);

	eyePos = glm::vec3(0.0f, 1.0f, 4.0f);
	eyeTarget = glm::vec3(0.0f, 0.0f, 3.0f);
	upDir = glm::vec3(0.0f, 1.0f, 0.0f);

	lightPos = glm::vec4(0.0f, 10.0f, 0.0f, 1.0f);
	rt3d::setLight(shaderProgram, light0);

	// set up projection matrix
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,50.0f);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));

	//meshObjects[0] = rt3d::createMesh(cubeVertCount, cubeVerts, nullptr, cubeVerts, cubeTexCoords, cubeIndexCount, cubeIndices);
}

glm::vec3 moveForward(glm::vec3 cam, GLfloat angle, GLfloat d) 
{
	return glm::vec3(cam.x + d*std::sin(angle*0.017453293), cam.y, cam.z - d*std::cos(angle*0.017453293));
}

glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) 
{
	return glm::vec3(pos.x + d*std::cos(angle*0.017453293), pos.y, pos.z + d*std::sin(angle*0.017453293));
}

void draw(SDL_Window * window) {
	// clear the screen
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	eyeTarget = moveForward(eyePos, rotation, 1.0f);
	glm::mat4 viewMat = glm::lookAt(eyePos, eyeTarget, upDir);

	glm::vec4 lightPosMat = viewMat * lightPos;
	rt3d::setLightPos(shaderProgram, glm::value_ptr(lightPosMat));

	for(int i=0; i<6; i++)
		cubes[i]->render(shaderProgram, viewMat);

	base->render(shaderProgram, viewMat);

    SDL_GL_SwapWindow(window); // swap buffers
}

void update()
{
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	if(keys[SDL_SCANCODE_LEFT]) lightPos.x -= 0.1f;
	if(keys[SDL_SCANCODE_RIGHT]) lightPos.x += 0.1f;
	if(keys[SDL_SCANCODE_UP]) lightPos.y += 0.1f;
	if(keys[SDL_SCANCODE_DOWN]) lightPos.y -= 0.1f;
	if(keys[SDL_SCANCODE_RCTRL]) lightPos.z -= 0.1f;
	if(keys[SDL_SCANCODE_RSHIFT]) lightPos.z += 0.1f;

	if(keys[SDL_SCANCODE_KP_4]) rotation -= 1.0f;
	if(keys[SDL_SCANCODE_KP_6]) rotation += 1.0f;

	
	if(keys[SDL_SCANCODE_A]) eyePos = moveRight(eyePos, rotation, -0.1f);
	if(keys[SDL_SCANCODE_D]) eyePos = moveRight(eyePos, rotation, 0.1f);
	if(keys[SDL_SCANCODE_W]) eyePos = moveForward(eyePos, rotation, 0.1f);
	if(keys[SDL_SCANCODE_S]) eyePos = moveForward(eyePos, rotation, -0.1f);
	if ( keys[SDL_SCANCODE_R] ) eyePos.y += 0.1f;
	if ( keys[SDL_SCANCODE_F] ) eyePos.y -= 0.1f;
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}